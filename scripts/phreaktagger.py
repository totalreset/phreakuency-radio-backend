#!/bin/python3
# Phreaktagger - id3 tagger - phreakuency.net

## The program will set id3 "song" tag with the filename of the .mp3 file.

from mp3_tagger import MP3File, VERSION_1

import os

print("\nPhreaktagger v0.1")
print("\nStarting tagging...\n")

folder = "/path/to/songs/"
folder_2 = "/path/to/sets/"
songs = os.listdir(folder)
sets = os.listdir(folder_2)
song_path = ""
tagged_songs = 0

for song in songs:
    try:
        song_path = folder + song
        mp3 = MP3File(song_path)
        mp3.set_version(VERSION_1)
        mp3.song = song.removesuffix(".mp3")
        mp3.save()
        tagged_songs += 1
        print(mp3.song)
        print("Song tagged.\n")
    except:
        print("Not a .mp3 file, skipping.")
        pass

for s in sets:
    try:
        song_path = folder_2 + s
        mp3 = MP3File(song_path)
        mp3.set_version(VERSION_1)
        mp3.song = s.removesuffix(".mp3")
        mp3.save()
        tagged_songs += 1
        print(mp3.song)
        print("Set tagged.\n")
    except:
        print("Not .mp3 format, skipping.\n")
        pass

print("\nThe total number of tagged songs and set is " + str(tagged_songs))
print("\nTagging complete. Thanks for using phreaktagger.")
