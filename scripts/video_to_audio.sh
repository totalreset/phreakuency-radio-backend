#!/usr/bin/env bash 

set -eux

rtmp_url='rtmp://totalreset.tk/phreak/mount'
liquidsoap_input='icecast://usernam:password@icecast.totalreset.tk:port/mountpoint'

while [ true ]
do

ffmpeg -i $rtmp_url -vn -acodec libvorbis -content_type application/ogg -f ogg $liquidsoap_input

sleep 5

done
