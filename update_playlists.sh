#!/usr/bin/bash

## This will just start the two scripts to update the playlists "day" and "night", also knowned as "Sets" and "Tracks".
set -e


if
  cd /radio/path/phreakuencyradiodata/sets/
  bash updateplaylist.sh &&
  cd /radio/path/phreakuencyradiodata/tracks/
  bash updateplaylist.sh
then 
  echo Went all good and playlists are updated!
else
  echo Something went bad.
  exit 23
fi

exit 0
